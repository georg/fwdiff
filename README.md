# bffdiff
A free software binary fast diff mainly for firmware.

## Description
Bffdiff prints a visual colored diff of two firmware binarys (usually some MB in size). It finds same sections even if they have been moved.

## Usage
```
Usage: bffdiff [OPTION] FILE1 [FILE2]

-d    --debug             print debug information
-h    --help              print this help
-o    --output-size LEN   set display / entropy graph length
                          with LEN out of: short, default, long
-v    --verbose           output verbose informations

Prints a visual colored diff of two firmware binarys (usually some MB in size).
Each file is displayed as entropy graph:
  . means zero entropy (padding)
  - means some entropy
  ~ means some more entropy
  + means medium entropy
  = means high entropy
  # means highest entropy (compressed streams)

Sections with same content are colored (even then they are moved).
```
## Screenshots
Entropy graph with same bytes colored in green:
![Screenshot](img/screenshot1.png "bffdiff screenshot")

## Authors and acknowledgment
Key algorithm is from Andrew Tridgells spamsum, see https://www.samba.org/ftp/unpacked/junkcode/spamsum/

A complete explanation of context triggered piecewise hashes (CTPH) can be found in http://dx.doi.org/10.1016/j.diin.2006.06.015

## Former name fwdiff
This project was renamed because the name was already used by others.

## License
GNU GPLv3
