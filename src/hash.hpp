//  bffdiff -- A free software firmware and binary diff
//  Copyright (C) 2023 Georg Gottleuber
//  this is code uses some lines of spamsum by Andrew Tridgell:
//  Copyright (C) 2002 Andrew Tridgell <tridge@samba.org>
//
//
//  This file is part of bffdiff
//
//  bffdiff is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bffdiff is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bffdiff.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_HASH_HPP
#define HEADER_HASH_HPP

#include <unistd.h> // getopt
#include <getopt.h>
#include <sys/file.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <iostream>
#include <iomanip> // setw
#include <fstream>
#include <map>
#include <unordered_map>
#include <math.h>
#include <vector>
#include <cstdint>

const uint32_t ROLLING_WINDOW = 7;

// "magic" initial hash value for FNV-1a, see
// https://de.wikipedia.org/wiki/FNV_(Informatik)
const uint64_t HASH_INIT = 0xcbf29ce484222325;

static struct {
	uint8_t window[ROLLING_WINDOW];
	uint32_t h1, h2, h3;
	uint32_t n;
} roll_state;

/* rolling hash implementation from spamsum.c (Andrew Tridgell) */
static inline uint32_t roll_hash(uint8_t c) {
	roll_state.h2 -= roll_state.h1;
	roll_state.h2 += ROLLING_WINDOW * c;

	roll_state.h1 += c;
	roll_state.h1 -= roll_state.window[roll_state.n % ROLLING_WINDOW];

	roll_state.window[roll_state.n % ROLLING_WINDOW] = c;
	roll_state.n++;

	roll_state.h3 = (roll_state.h3 << 5) & 0xFFFFFFFF;
	roll_state.h3 ^= c;

	return roll_state.h1 + roll_state.h2 + roll_state.h3;
}

static uint32_t roll_reset(void) {
	memset(&roll_state, 0, sizeof(roll_state));
	return 0;
}

// a simple non-rolling hash: FNV-1a from
// https://de.wikipedia.org/wiki/FNV_(Informatik)
static inline uint64_t sum_hash(const uint8_t c, const uint64_t h) {
	const uint64_t hash_prime = 0x00000100000001b3;
	return (h ^ c) * hash_prime;
}

#endif
