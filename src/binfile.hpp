//  bffdiff -- A free software firmware and binary diff
//  Copyright (C) 2023 Georg Gottleuber
//
//
//  This file is part of bffdiff
//
//  bffdiff is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bffdiff is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bffdiff.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_BINFILE_HPP
#define HEADER_BINFILE_HPP

#include <sys/file.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <iostream>
#include <iomanip> // setw
#include <fstream>
#include <map>
#include <unordered_map>
#include <math.h>
#include <vector>
#include <cstdint>

using namespace std;

enum Output_Size {
	SHORT = 0,
	DEFAULT = 1,
	LONG = 2
};

enum Report_Mode {
	NONE = 0,
	SAME = 1,
	DIFF = 2
};

struct options {
  int benchmark_sec = 0;
  int verbose = 0;
  int debug = 0;
  Output_Size output_size = DEFAULT;
  Report_Mode report_mode = NONE;
  bool usage = false;
  bool single_file_mode = false;
};

class Binfile {
	public:
		string filename;
		options opts;
		vector<byte> data;
		streamsize filesize;
		uint64_t disp_block_size;

		// histogram of complete file
		vector<uint64_t> histogram;

		// entropy per byte: between zero and eight
		vector<double> entropy_vec;

		// key: ctph (context triggered picewise hash); value: pos and length
		unordered_map<uint64_t, pair<uint64_t, uint64_t>> ctph;

		// same data inside of blocks (disp_block_size) in percent 0-100
		vector<int8_t> same_blocks;

		Binfile(ifstream &i, const string filename, const options opts, uint64_t disp_block_size);
		void analyze(uint64_t max_file_size, bool do_ctph);
		void print_entropy_graph() const;
		void print_entropy_line(uint64_t line) const;
};

#endif
