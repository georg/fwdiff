//  bffdiff -- A free software firmware and binary diff
//  Copyright (C) 2023 Georg Gottleuber
//  this is code uses some lines of spamsum by Andrew Tridgell:
//  Copyright (C) 2002 Andrew Tridgell <tridge@samba.org>
//
//
//  This file is part of bffdiff
//
//  bffdiff is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bffdiff is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bffdiff.  If not, see <http://www.gnu.org/licenses/>.

#include <unistd.h> // getopt
#include <getopt.h>
#include <sys/file.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <iostream>
#include <iomanip> // setw
#include <fstream>
#include <map>
#include <unordered_map>
#include <math.h>
#include <vector>

#include "main.hpp"

#define PACKAGE_NAME "bffdiff"

// block size for entropy graph in bytes (will be shown as one char)
static uint64_t disp_blk_size = 512;

using namespace std;

void print_usage(void) {
	cout	<< PACKAGE_NAME << " " << PACKAGE_VERSION << endl
			<< "Usage: " << PACKAGE_NAME << " [OPTION] FILE1 [FILE2]\n\n"

			<< "-d    --debug             print debug information\n"
			<< "-h    --help              print this help\n"
			<< "-o    --output-size LEN   set display / entropy graph length\n"
			<< "                          with LEN out of: short, default, long\n"
			<< "-r    --report same|diff  report same or different data\n\n"
			<< "-v    --verbose           output verbose informations\n\n"

			<< "Prints a visual colored diff of two firmware binarys (usually some MB in size).\n"
			<< "Each file is displayed as entropy graph:\n"
			<< "  . means zero entropy (padding)\n"
			<< "  - means some entropy\n"
			<< "  ~ means some more entropy\n"
			<< "  + means medium entropy\n"
			<< "  = means high entropy\n"
			<< "  # means highest entropy (compressed streams)\n\n"

			<< "Sections with same content are colored (even then they are moved).\n"
			<< endl;
}

// find same bytes in memory with maximal search length (end of file)
uint64_t find_same_bytes(const void* buf1, const void* buf2, uint64_t max_search_len) {
	uint64_t same_count = 0;

	if (!max_search_len) {
		return same_count;
	}
	while (--max_search_len && *(uint8_t*)buf1 == *(uint8_t*)buf2 ) {
		same_count++;
		buf1 = (uint8_t*)buf1 + 1;
		buf2 = (uint8_t*)buf2 + 1;
	}
	return same_count;
}

// find same bytes backwards in memory with maximal search length (end of file)
uint64_t find_same_bytes_backward(const void* buf1, const void* buf2, uint64_t max_search_len) {
	uint64_t same_count = 0;

	if (!max_search_len) {
		return same_count;
	}

	buf1 = (uint8_t*)buf1 - 1;
	buf2 = (uint8_t*)buf2 - 1;

	while (--max_search_len && *(uint8_t*)buf1 == *(uint8_t*)buf2 ) {
		same_count++;
		buf1 = (uint8_t*)buf1 - 1;
		buf2 = (uint8_t*)buf2 - 1;
	}
	return same_count;
}

void compare_files(Binfile &b1, Binfile &b2, const options opts) {
	const int FIRST_COL_LEN = 72;

	vector<Same_Region> same_regions;
	uint64_t same_regions_count = 0;

	// we try to fill a same_blocks vectors for both b1 and b2. If both are at filesize, we are done.
	for(unordered_map<uint64_t, pair<uint64_t, uint64_t>>::iterator iter = b1.ctph.begin(); iter != b1.ctph.end(); ++iter) {
		uint64_t k =  iter->first;
		uint64_t offset =  iter->second.first;
		if (b2.ctph.contains(k)) {
			// find length of same byte stream
			// if (opts.debug)
			//	cout << "DEBUG: Found same hash " << hex <<  k << dec << " at b2 pos " << b2.ctph[k].first << " with len "<< b2.ctph[k].second << '\n';

			uint64_t b2offset = b2.ctph[k].first;
			// is there already a match? locate offset in same_blocks
			if (b1.same_blocks[offset/disp_blk_size] < 0) {
				// calculate min(b1, b2) distance to file end
				uint64_t max_search_len = min(b1.filesize - offset, b2.filesize - b2offset);
				uint64_t same_len = find_same_bytes(&(b1.data[offset]), &(b2.data[b2offset]), max_search_len);

				// find same bytes before rolling hash triggerd
				max_search_len = min(offset, b2offset);
				uint64_t same_len_backward = find_same_bytes_backward(&(b1.data[offset]), &(b2.data[b2offset]), max_search_len);

				// calculate normalized offsets (same_len_backward = 0)
				uint64_t norm_offset = offset - same_len_backward;
				uint64_t norm_b2offset = b2offset - same_len_backward;
				uint64_t norm_same_len = same_len + same_len_backward;

				// large junk of same data -> create sameblock element
				if (norm_same_len > 4096) {

					// search for same entry already in vector (FIXME this should not happen; fix this performance issue)
					if (same_regions_count > 0 &&
						same_regions[same_regions_count-1].pos_f1 == norm_offset &&
						same_regions[same_regions_count-1].pos_f2 == norm_b2offset &&
						same_regions[same_regions_count-1].offset_to_f1 == static_cast<int64_t>(norm_b2offset - norm_offset) &&
						same_regions[same_regions_count-1].len == norm_same_len ) {
						if (opts.debug)
							cout << "WARNING: found same block again; please fix this" << endl;
					} else {
						same_regions.emplace_back(norm_offset, norm_b2offset, norm_b2offset - norm_offset, norm_same_len);
						same_regions_count++;
					}
				}

				// large junk of same data -> mark it in same_blocks
				if (norm_same_len > disp_blk_size*2) {
					// try to calculate how much is inside a display block
					for (uint64_t blkc = 0; blkc <= norm_same_len / disp_blk_size; blkc++) {
						// secial calc for starting block
						if (blkc == 0) {
							uint64_t tmp_percent = ((disp_blk_size - (norm_offset % disp_blk_size)) * 100) / disp_blk_size;
							if (tmp_percent > 100 )
								cout << "Error: calculation fail for b1: more than 100 percent; tmp_percent = " << tmp_percent << '\n';
							else
								b1.same_blocks[norm_offset/disp_blk_size + blkc] = static_cast<int8_t>(tmp_percent);

							tmp_percent = ((disp_blk_size - (norm_b2offset % disp_blk_size)) * 100) / disp_blk_size;
							if (tmp_percent > 100 )
								cout << "Error: calculation fail for b2: more than 100 percent; tmp_percent = " << tmp_percent << '\n';
							else
								b2.same_blocks[norm_b2offset/disp_blk_size + blkc] = static_cast<int8_t>(tmp_percent);
						}

						b1.same_blocks[norm_offset/disp_blk_size + blkc] = 100;
						b2.same_blocks[norm_b2offset/disp_blk_size + blkc] = 100;
					}
				}
			}
		}
	}

	cout << "visual file difference for files (equal 100%: green; >20%: cyan):" << endl;
	cout << b1.filename << " ";

	// file filenames line with spaces to match colomn
	int64_t fill = FIRST_COL_LEN + 1 - b1.filename.length();
	while (fill > 0) {
		cout << " ";
		fill--;
	}

	cout << b2.filename;

	for (uint64_t line = 0; line < max(b1.entropy_vec.size(), b2.entropy_vec.size()); line += EG_CHAR_PER_LINE) {
		cout << endl;
		// TODO autoscale setw(x)
		cout << setw(5) << (line * disp_blk_size) / 1024 << "K: ";
		b1.print_entropy_line(line);
		// space between both entropy listings
		cout << "  ";
		b2.print_entropy_line(line);
	}
	cout << endl;

	// print number of green blocks per file
	if (opts.debug) {
		cout << "same: ";
		uint64_t count_same_blocks = 0;
		for (uint64_t blk = 0; blk < b1.same_blocks.size(); blk++) {
			if (b1.same_blocks[blk] == 100 )
				count_same_blocks++;
		}
		cout << setw(32) << count_same_blocks << "  ";
		count_same_blocks = 0;
		for (uint64_t blk = 0; blk < b2.same_blocks.size(); blk++) {
			if (b2.same_blocks[blk] == 100 )
				count_same_blocks++;
		}
		cout << setw(64) << count_same_blocks << "  ";
		cout << endl;
	}

	if (opts.verbose > 0) {
		cout << "Statistics: " << endl;
		cout << "filesize: " <<  b1.filesize << " Bytes" << setw(64) << b2.filesize << " Bytes" << endl;
		// TODO:
		// for each: usage for data bytes and percent (rest ist empty)
		// for each: empty space data bytes and percent
		//           most common used Symbols (0x00, 0xff, 0xf0, etc)
		//
		// for each: same data found in bytes and percent
		// combined: list of 10 biggest blocks with offset
	}
	if (opts.report_mode == SAME) {
		cout << "Report same regions: " << endl;
		for (uint64_t i=0; i < same_regions_count; i++) {
			struct Same_Region *b = &(same_regions[i]);
			cout << "Same block at 0x"<< hex << setw(8) << setfill('0') << b->pos_f1 << " with len " << dec << setw(6) << setfill(' ') << b->len << " bytes; offset to second file " << b->offset_to_f1 << " byte" << endl;
		}
	}
	if (opts.report_mode == DIFF) {
		cout << "Report different regions: Sorry: Not implemented yet :-(" << endl;
	}
}

int process_options(int argc, char* argv[], options& opts) {
	int option_index = 0;
	int error = 0;
	for(;;)
	{
		// options for getopt
		static struct option long_options[] =
		{
			{"debug", no_argument, NULL, 'd'},
			{"help", no_argument, NULL, 'h'},
			{"output-size", required_argument, NULL, 'o'},
			{"verbose", no_argument, NULL, 'v'},
			{NULL, 0, NULL, 0}
		};
		int c = getopt_long(argc, argv, "dho:r:v::", long_options, &option_index);
		if (c == -1)
			break;
		switch (c)
		{
			case 'd':
				opts.debug++;
				break;
			case 'h':
				opts.usage = true;
				break;
			case 'o':
				if (optarg == NULL) {
					cerr << "ERROR: Missing output size" << endl;
					error = 1;
				}
				else {
					if (string(optarg) == "long") {
						opts.output_size = LONG;
					}
					else if (string(optarg) == "default") {
						opts.output_size = DEFAULT;
					}
					else if (string(optarg) == "short") {
						opts.output_size = SHORT;
					}
					else {
						cerr << "ERROR: Unrecognized output size " << optarg << endl;
						error = 1;
					}
				}
				break;
			case 'r':
				if (optarg == NULL) {
					cerr << "ERROR: Missing report mode: \"same\" or \"diff\"" << endl;
					error = 1;
				}
				else {
					if (string(optarg) == "same") {
						opts.report_mode = SAME;
					}
					else if (string(optarg) == "diff") {
						opts.report_mode = DIFF;
					}
					else {
						cerr << "ERROR: Unrecognized report mode: use \"same\" or \"diff\"" << optarg << endl;
						error = 1;
					}
				}
				break;
			case 'v':
				opts.verbose++;
				break;
			default:
				error = 1;
		}
	}
	if (opts.usage == false) {
		if (argc - optind < 1 ) {
			cerr	<< "Error: too few arguments given (at least one file must be named)" << endl;
			error = 1;
		}
		if (argc - optind == 1 ) {
			opts.single_file_mode = true;
			cout << "only one file given; switch to single file mode" << endl;
		}
		if (argc - optind > 2 ) {
			cerr	<< "Error: too many arguments given (only two files are allowed)" << endl;
			error = 1;
		}
	}

	return error;
}

// to calculate optimal display / entropy block size: so entropy graph output adapts to file length
uint64_t calc_optimal_block_size(uint64_t filesize, Output_Size output_size, options opts) {
	const uint64_t default_output_len = 80;
	if (output_size == LONG)
		// below 512 bytes statistical errors in entropy calculation leads to unstable results
		disp_blk_size = 512;
	if (output_size == SHORT)
		disp_blk_size = 4096;
	if (output_size == DEFAULT) {
		while (filesize / EG_CHAR_PER_LINE / disp_blk_size > default_output_len)
			disp_blk_size += 512;

		if (opts.debug)
			cout << "DEBUG: choose display / entropy graph block size: " << disp_blk_size << " byte" << endl;
	}
	return disp_blk_size;
}

int main(int argc, char *argv[]) {
	options opts;
	int error = 0;
	size_t filesize1, filesize2, maxsize;

	error = process_options(argc, argv, opts);
	if (error)
		exit(error);
	if (opts.usage) {
		print_usage();
		exit(0);
	}

	// open and and check size of file1 and file2
	string fname1 = argv[optind];
	ifstream infile1(fname1.c_str(), ios::in|ios::binary|ios::ate);
	if (!infile1.is_open()) {
		cerr << "Error: Cannot open file: " << fname1.c_str() << endl;
		exit(2);
	}
	filesize1 = infile1.tellg();

	// only one file was given; print entropy graph and exit
	if (opts.single_file_mode) {
		const bool do_ctph = false;

		// create object and analyze file
		Binfile b1 = Binfile(infile1, fname1, opts, disp_blk_size);
		disp_blk_size = calc_optimal_block_size(filesize1, opts.output_size, opts);

		b1.analyze(filesize1, do_ctph);
		b1.print_entropy_graph();
		infile1.close();
		exit(0);
	}

	string fname2 = argv[optind + 1];
	ifstream infile2(fname2.c_str(), ios::in|ios::binary|ios::ate);
	if (!infile2.is_open()) {
		cerr << "Error: Cannot open file: " << fname2.c_str() << endl;
		infile1.close();
		exit(2);
	}
	filesize2 = infile2.tellg();

	// to calculate optimal display / entropy block size: so entropy graph output adapts to file length
	maxsize = max(filesize1, filesize2);
	disp_blk_size = calc_optimal_block_size(maxsize, opts.output_size, opts);

	// create object and analyze files
	Binfile b1 = Binfile(infile1, fname1, opts, disp_blk_size);
	b1.analyze(maxsize, true);
	infile1.close();

	Binfile b2 = Binfile(infile2, fname2, opts, disp_blk_size);
	b2.analyze(maxsize, true);
	infile2.close();

	compare_files(b1, b2, opts);
	exit(0);
}
