//  bffdiff -- A free software firmware and binary diff
//  Copyright (C) 2023 Georg Gottleuber
//  this is code uses some lines of spamsum by Andrew Tridgell:
//  Copyright (C) 2002 Andrew Tridgell <tridge@samba.org>
//
//
//  This file is part of bffdiff
//
//  bffdiff is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bffdiff is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bffdiff.  If not, see <http://www.gnu.org/licenses/>.

#include <unistd.h> // getopt
#include <getopt.h>
#include <sys/file.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <iostream>
#include <iomanip> // setw
#include <fstream>
#include <map>
#include <unordered_map>
#include <math.h>
#include <vector>

#include "hash.hpp"
#include "binfile.hpp"
#include "main.hpp"

#define COUT_GREEN "\033[32m"
#define COUT_CYAN "\033[36m"
#define COUT_DEFAULT "\033[39m"

// print entropy per byte
char entropy_to_char(const double v) {
	if (v < 0.01)
		return '.'; // [0, 0.01[
	if (v < 5)
		return '-'; // [0.01, 5[
	if (v < 6.4)
		return '~'; // [5, 6.4[
	if (v < 7.6)
		return '+'; // [6.4, 7.6[
	if (v < 7.85)
		return '='; // [7.6, 7.85[
	else
		return '#'; // [7.85, inf[
};

Binfile::Binfile(ifstream &i, const string filename, const options opts, const uint64_t disp_block_size)
		: filename(filename), opts(opts), disp_block_size(disp_block_size) {
	filesize = i.tellg();
	if (opts.verbose > 0)
		cout << "file size of "<< filename << " is " << filesize << " bytes" << endl;
	i.seekg(0, ios::beg);

	//alloc memory and read file to RAM
	data.reserve(filesize);
	if (!i.read(reinterpret_cast<char*>(data.data()), filesize)) {
		cout << "Error reading file " << filename << endl;
	}
	// init same blocks with -1 for "not tested"
	same_blocks.resize(filesize/disp_block_size);
	for(uint64_t i = 0; i < same_blocks.size(); i++)
		same_blocks[i] = -1;

	// init with 256 elements
	histogram.resize(0x100);
}

void Binfile::analyze(uint64_t max_file_size, bool do_ctph) {
	// rolling hash trigger size (maybe to choose not a prime is beneficial)
	// this decides how big the junks for traditional hashing will be
	uint64_t rht_size = max(static_cast<uint64_t>(6), 6 * (max_file_size / (1024 * 1024)) );
	uint32_t h = roll_reset();
	uint64_t h2 = HASH_INIT;
	uint64_t last_triggerd = 0;
	uint64_t trigger_count = 0;
	uint64_t hash_start = 0;
	if (do_ctph) {
		for (size_t i=0; i < static_cast<size_t>(filesize); i++) {
			uint8_t data_byte = static_cast<uint8_t>(data[i]);

			h = roll_hash(data_byte);
			h2 = sum_hash(data_byte, h2);

			// only end current chunk if length is bigger than 8 bytes; this drastically reduces collisions
			if ((i - last_triggerd >= 8) && (h % rht_size == rht_size-1)) {
				// we have hit a reset point. We now emit a hash which is based on all chacaters
				// in the piece of the message between the last reset point and this one
				//
				// TODO: what if key is already used? (bad hash distribution or same data in file?)
				if (opts.debug) {
					if (ctph.contains(h2)) {
						cout << "DEBUG WARNING: key already in ctph "<< hex << h2 << dec << "; len: " << i - last_triggerd<< endl;
					}
				}

				// save this hash and start pos
				ctph.insert(make_pair(h2, make_pair(hash_start, i - last_triggerd)));
				if (opts.debug) {
					if (i -last_triggerd > 1000000) {
						cout << "DEBUG: insert key: 0x"<< hex << h2 << " start 0x"<< hash_start << " hashlen " << dec << i - last_triggerd << endl;
					}
				}

				hash_start = i+1;

				// reset non rolling hash
				h2 = HASH_INIT;
				last_triggerd = i;
				trigger_count++;
			}
		}
		if (opts.debug) {
			cout << "DEBUG: End rolling hash: trigger count: "<< trigger_count << "; all hash size "<< trigger_count * sizeof(uint64_t) << " Bytes" << endl;
			cout << "DEBUG: CTPH hashtable has "<< ctph.size() << " entrys; taking "<< ctph.size() * 3 * sizeof(uint64_t) << " Bytes" << endl;
		}
	}

	// walk this binary and
	// * add up histogram values (complete file)
	// * entropy of every 1k block
	uint64_t dataval = 0xfff;
	vector<uint64_t> histogram_1k;
	histogram_1k.resize(1<<8);
	for (size_t i=0; i < static_cast<size_t>(filesize); i++) {
		// histogram
		dataval = static_cast<uint64_t>(data[i]);
		histogram[dataval] += 1;

		// calc histogram in disp_block_size
		if ((i % disp_block_size) == 0 && i != 0) {
			double entropy_1k = 0 ;
			// calculate entropy
			for (int j=0; j <= 0xff; j++) {
				double freq = static_cast<double>(histogram_1k[j]) / static_cast<double>(disp_block_size) ;
				// avoid turning block to nan
				if (freq != 0.0)
					entropy_1k -= freq * log2( freq );
			}
			// save entropy
			entropy_vec.push_back(entropy_1k);

			// reset to zero
			fill(histogram_1k.begin(), histogram_1k.end(), 0);
		}
		histogram_1k[dataval] += 1;

		// TODO: output min and max entropy for debug
	}

	if (opts.verbose > 0) {
		std::cout << "Histogram of " << filename << ":" << endl;
		std::cout << "val occurence" << endl;
		for (int i=0; i <= 0xff; i++) {
			cout << setw(3) << i << " "<< histogram[i] << endl;
		}
		cout << endl << endl;
	}

}

void Binfile::print_entropy_graph() const {
	for (uint64_t line = 0; line < entropy_vec.size(); line += EG_CHAR_PER_LINE) {
		cout << endl;
		// TODO autoscale setw(x)
		cout << setw(5) << (line * disp_block_size) / 1024 << "K: ";
		print_entropy_line(line);
	}
	cout << endl;
}

void Binfile::print_entropy_line(const uint64_t line) const {
	for (int sym = 0; sym < EG_CHAR_PER_LINE; sym++) {
		if (line + sym + 1 >= entropy_vec.size()) {
			cout << " ";
		}
		else {
			double sum = entropy_vec[line+sym];
			if (line + sym < same_blocks.size() && same_blocks[line + sym] == 100)
				cout << COUT_GREEN << entropy_to_char(sum) << COUT_DEFAULT;
			else {
				if (line + sym < same_blocks.size() && same_blocks[line + sym] > 20)
					cout << COUT_CYAN << entropy_to_char(sum) << COUT_DEFAULT;
				else
					cout << entropy_to_char(sum);
			}
		}
	}
}

