//  bffdiff -- A free software firmware and binary diff
//  Copyright (C) 2023 Georg Gottleuber
//
//  This file is part of bffdiff
//
//  bffdiff is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bffdiff is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bffdiff.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_MAIN_HPP
#define HEADER_MAIN_HPP

#include <unistd.h> // getopt
#include <getopt.h>
#include <sys/file.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <iostream>
#include <iomanip> // setw
#include <fstream>
#include <map>
#include <unordered_map>
#include <math.h>
#include <vector>

#include "binfile.hpp"
#include "version.hpp"

// entropy graph line length per file (length of a file row)
#define EG_CHAR_PER_LINE 64

struct Same_Region {
	uint64_t pos_f1;
	uint64_t pos_f2;
	int64_t offset_to_f1;
	uint64_t len;

    Same_Region(uint64_t p_pos_f1, uint64_t p_pos_f2, int64_t p_offset_to_f1, uint64_t p_len)
        : pos_f1(p_pos_f1), pos_f2(p_pos_f2), offset_to_f1(p_offset_to_f1), len(p_len)
    { }
};

#endif
