#!/bin/python3
#
# fwdiff -- A free software firmware and binary diff
# Copyright (C) 2023 Georg Gottleuber
#
# This file is part of fwdiff
#
# fwdiff is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fwdiff is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fwdiff.  If not, see <http://www.gnu.org/licenses/>.
#
#
# This script generates multiple test files with different offsets and
# entropy patterns:

import random

def make_testfiles(size, filename):
    # 12 -> 4KB, 13 -> 8KB, 14 -> 16KB, ...
    # not a power of two (aligned data would be easy)
    testpattern_size = pow(2,size) - 3

    some_random_offset = random.randint(1,4095)
    print("random offset is: ", some_random_offset)
    allzero = [0 for _ in range(some_random_offset)]

    # create test files
    f0 = open("testfile_" + filename + "0", "wb")
    f1 = open("testfile_" + filename + "1", "wb")

    # write to file
    for byte in allzero:
        f0.write(byte.to_bytes(1, byteorder='big'))

    for entropy in range(101):
        # create data with well defined entropy (percent steps)
        entropy_data = [random.randint(0,int(255*entropy/100.0)) for _ in range(testpattern_size)]

        # make every second block not the same in both test files
        if entropy % 2 == 0:
            for byte in entropy_data:
                f0.write(byte.to_bytes(1, byteorder='big'))
                f1.write(byte.to_bytes(1, byteorder='big'))

        else:
            for byte in entropy_data:
                f0.write(byte.to_bytes(1, byteorder='big'))

            # different entropy data
            diff_entropy_data = [random.randint(0,int(255*entropy/100.0)) for _ in range(testpattern_size)]
            for byte in diff_entropy_data:
                f1.write(byte.to_bytes(1, byteorder='big'))

    f0.close()
    f1.close()
    print("generated testfiles: \"testfile_" + filename + "0\" and \"testfile_" + filename + "0\"")


make_testfiles(13, "small")  # -> 800 KB
make_testfiles(16, "medium") # ->   6 MB
#make_testfiles(19, "large") # ->  51 MB (may take some time)
