#!/bin/bash

# build bffdiff

# test if build dir is already there
if [[ -d ./build ]] ; then
	# build dir is present: just call make
	cd build && make && cd ..
else
	# create build dir and init cmake
	mkdir build && cd build && cmake .. && make && cd .. && echo "successfully compiled build/bffdiff"
fi
